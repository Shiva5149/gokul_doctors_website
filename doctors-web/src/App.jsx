import logo from './logo.svg';
import './App.css';
import {
  Grid,
 Typography,
} from '@material-ui/core';
import NavBar from './components/homepage/navbar';
import HomePage1 from './components/homepage/homepage-module1';
import HomePage2 from './components/homepage/homepage-module2';
import HomePage3 from './components/homepage/homepage-module3';
import HomePage4 from './components/homepage/homepage-module4';
import HomePage5 from './components/homepage/homepage-module5';
import HomePage6 from './components/homepage/homepage-module6';
import ContactUs from './components/homepage/contactuspage-module';
import ClientTalk from './components/homepage/clienttalk';

function App() {
  return (
    <Grid className="App">
      <NavBar/>
      <HomePage1/>
      <HomePage2/>
      <HomePage3/>
      <HomePage4/>
      <HomePage5/>
      <ContactUs/>
      {/* <ClientTalk/> */}
      <HomePage6/>
    </Grid>
  );
}

export default App;