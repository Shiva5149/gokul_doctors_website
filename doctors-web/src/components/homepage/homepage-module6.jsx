import React from "react";
import {
     Grid,
     FooterContainer,
     List,
      ListItem,
      ListItemLink,
      ListItemText,
    Typography,
 } from '@material-ui/core';
 import FacebookIcon from '@material-ui/icons/Facebook';
 import TwitterIcon from '@material-ui/icons/Twitter';
 import LinkedInIcon from '@material-ui/icons/LinkedIn';
 import YouTubeIcon from '@material-ui/icons/YouTube';
 import InstagramIcon from '@material-ui/icons/Instagram';

export default function HomePage6() {
    return(
        <Grid  className='Homepage6'>
            <Grid container direction="row" lg={12} md={12} xs={12} spacing={2} className='Homepage6-list'>
            <Grid item lg={3}>
                <img src="https://corf-react.envytheme.com/img/logo.png"/>
                <Typography>
                    Lorem ipsum dolor, sit amet earum consectetur adipisicing elit. Cupiditate rerum quidem fugiat sapiente! Iusto quae perspiciatis, repudiandae ipsam minus et ex, aliquid dolor molestias, earum enim officiis porro obcaecati.
                </Typography>
                <FacebookIcon/><TwitterIcon/><LinkedInIcon/><YouTubeIcon /><InstagramIcon />
            </Grid>
             <Grid item lg={3}>
                
                <Typography>
                    <h3>Department</h3>
                </Typography>
                <List>
                  <ListItemText primary="Surgery & Radiology"/>
                  <ListItemText primary="Children Care"/>
                  <ListItemText primary="Orthopedics"/> 
                  <ListItemText primary="Nuclear Magnetic"/>
                  <ListItemText primary="Eye Treatment"/>
                   <ListItemText primary="X-Ray"/>
                </List>
            </Grid>
            <Grid item lg={3}>
                <h3>Opening Hours</h3>
                <List>
                  <ListItemText/><span>Mon-Tue:</span><span class="right">6:00AM-10:00PM</span>
                  <ListItemText/><span>Wed-Thu:</span><span class="right">6:00AM-10:00PM</span>
                  <ListItemText/><span>Fry:</span> <span class="right">6:00AM-10:00PM</span>
                  <ListItemText/><span>Sun:</span><span class="right">Closed</span>
                 
                </List>
            </Grid>
            <Grid item lg={3}>
                <h3>Get in Touch</h3>
                <List>
                  <ListItemText/><span>Mon-Tue:</span><span class="right">6:00AM-10:00PM</span>
                  <ListItemText/><span>Wed-Thu:</span><span class="right">6:00AM-10:00PM</span>
                  <ListItemText/><span>Fry:</span> <span class="right">6:00AM-10:00PM</span>
                  <ListItemText/><span>Sun:</span><span class="right">Closed</span>
                 
                </List>
            </Grid>
        </Grid>
        </Grid>
    );
}