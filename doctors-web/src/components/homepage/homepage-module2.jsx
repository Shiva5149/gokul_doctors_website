import React from "react";
import {
     Grid,
    Typography,
    Button,
    Card,
    CardActions,
    CardContent,

 } from '@material-ui/core';
 import AddIcon from '@material-ui/icons/Add';
 import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
 import AirplanemodeActiveIcon from '@material-ui/icons/AirplanemodeActive';
 import ComputerIcon from '@material-ui/icons/Computer';
import './styles.scss';


export default function HomePage2() {
    return(
        <Grid container direction="row" lg={12} md={12} xs={12} className='Homepage2'>
            
               <Grid item md={4} xs={12} lg={4}>
                <Card className='Homepage2-card'>
                <CardContent >
                
                    <Typography variant='h5' className='Homepage-card-title' gutterBottom>
                  <img src="https://corf-react.envytheme.com/img/facility-img/facility-icon1.png" class="icon"/><br/> <strong>Qualified Doctors</strong>
                    </Typography>
                    <Typography className='Homepage2-card-pos' color="textSecondary">
                    Lorem ipsum dolor sit amet,<br/>
                     consectetur adipiscing elit, sed do
                    </Typography>
                </CardContent>
                
                <CardActions>
                    <Button size="small" style={{color:"#6CCDCB",marginLeft:"41%",}}>READ<AddIcon /></Button>
                </CardActions>
                </Card>
              </Grid>
              <Grid item md={4} xs={12} lg={4}>
                <Card className='Homepage2-card'>
                <CardContent >
                
                    <Typography variant='h5' className='Homepage-card-title' gutterBottom>
                  <img src="https://corf-react.envytheme.com/img/facility-img/facility-icon2.png" class="icon"/><br/> <strong>Emergency Helicopter</strong>
                    </Typography>
                    <Typography className='Homepage2-card-pos' color="textSecondary">
                    Lorem ipsum dolor sit amet,<br/>
                     consectetur adipiscing elit, sed do
                    </Typography>
                </CardContent>
                
                <CardActions>
                    <Button size="small" style={{color:"#6CCDCB",marginLeft:"41%",}}>READ<AddIcon /></Button>
                </CardActions>
                </Card>
              </Grid>
              <Grid item md={4} xs={12} lg={4}>
                 <Card className='Homepage2-card'>
                <CardContent >
                
                    <Typography variant='h5' className='Homepage-card-title' gutterBottom>
                  <img src="https://corf-react.envytheme.com/img/facility-img/facility-icon3.png" class="icon" /><br/> <strong>Leading Technology</strong>
                    </Typography>
                    <Typography className='Homepage2-card-pos' color="textSecondary">
                    Lorem ipsum dolor sit amet,<br/>
                     consectetur adipiscing elit, sed do
                    </Typography>
                </CardContent>
                
                <CardActions>
                    <Button size="small" style={{color:"#6CCDCB",marginLeft:"41%",}}>READ<AddIcon /></Button>
                </CardActions>
                </Card>
              </Grid>
        </Grid>
    );
}