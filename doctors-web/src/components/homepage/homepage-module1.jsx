import React from "react";
import {
    List,
      ListItem,
      ListItemLink,
      ListItemText,
     Grid,
    Typography,
    Button,
    Card,
    CardActions,
    CardContent,

 } from '@material-ui/core';
 import './styles.scss';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import AirportShuttleIcon from '@material-ui/icons/AirportShuttle';
import BeenhereIcon from '@material-ui/icons/Beenhere';
import imagehome1 from '../images/image-home1.png';
import spinImage from '../images/spin-image.png';
import HeadsetMicTwoToneIcon from '@material-ui/icons/HeadsetMicTwoTone';
 import { Navbar } from 'react-bootstrap';
 import logo from '../images/logo.png';
 import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
  }));


export default function HomePage1() {
    const classes = useStyles();


    
    return(
        <Grid container direction="row" lg={12} md={12} xs={12} className='Homepage1'>
             <AppBar className='Homepage1-toolBar' style={{backgroundColor: "inherit", top: "80px", position:'absolute',}}>
            <Toolbar variant="dense">
            <IconButton edge="start" className={classes.menuButton} color="black" aria-label="menu">
                <img src={logo} alt="" className='Homepage1-image'/>
            </IconButton>
            <Typography variant="h6" color="inherit">
                Home
            </Typography>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Typography variant="h6" color="inherit">
                Services
            </Typography>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Typography variant="h6" color="inherit">
                Doctors
            </Typography>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Typography variant="h6" color="inherit">
                About
            </Typography>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Typography variant="h6" color="inherit">
                Contact
            </Typography>
            </Toolbar>
        </AppBar>
            <Grid  item lg={4} md={6} xs={6} spacing={4}  className='Hompage1-Text'>
                <Typography variant='h6' style={{color:'blue'}}><strong>Smarter Service Care</strong></Typography><br/>
                <Typography variant='h3' style={{}}><strong>We are Committed </strong> </Typography><br/>
                <Typography variant='h3' style={{}}><strong>to Your Best Health</strong> </Typography><br/>
                <Typography>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br/>
                     tempor incididunt ut labore et dolore magna aliqua gravida. Risus commodo.</Typography><br/>
                <Button 
                color='blue'
                variant='contained'

                >
                    Book an Appointment
                </Button>
                &nbsp;&nbsp;&nbsp;
                <Button
                color='blue'
                variant='contained'
                >Play Now</Button>
            </Grid>
            <Grid  item lg={6} md={4} xs={6} className='Homepage1-img'>
            <img
             style={{
                 width: "auto", 
                 height: "526px",
                 zIndex: "999",
                 position: "relative",
                 alignItems:'right',
                 justifyContent:'right',
                 maxWidth: 'unset',
                 marginLeft: "78px",
                marginTop: "-21px",

                 }} 
                 src={imagehome1} 
                 alt={imagehome1}
                 
                 />
            <img
             style={{
                 animation: `spin 25s linear infinite`,
                position: 'unset',
                width: "auto", 
                height: '657px',
                marginTop: "-63%",
                
                }} 
                src={spinImage} 
                alt="img"
                />
            </Grid>
            <Grid container direction='row' spacing={3} item lg={10} md={10} md={12} xs={12}>
            <Card className='Homepage1-card' style={{marginTop: '30px',marginLeft: '10px',backgroundColor: "#d6fbfd" ,}}>
                <CardContent >
                    <Typography variant='h5' className='Homepage-card-title' gutterBottom>
                    <LocalHospitalIcon className='Homepage-icon'/><strong>Special Service</strong>
                    </Typography>
                    <Typography className='Homepage1-card-pos' color="textSecondary">
                    Lorem ipsum dolor sit amet,<br/>
                     consectetur adipiscing elit, sed do
                    </Typography>
                </CardContent>
                
                {/* <CardActions>
                    <Button size="small">Learn More</Button>
                </CardActions> */}
                </Card>
                <Card className='Homepage1-card' style={{marginTop: '30px',marginLeft: '-10px',backgroundColor: "#d6fbfd" ,}}>
                <CardContent >
                   <Typography variant='h5' className='Homepage-card-title' gutterBottom>
                    <HeadsetMicTwoToneIcon className='Homepage-icon' /><strong>24/7 Advanced Care</strong>
                    </Typography>
                    <Typography className='Homepage1-card-pos' color="textSecondary">
                    Lorem ipsum dolor sit amet,<br/>
                     consectetur adipiscing elit, sed do
                    </Typography>
                </CardContent>
                
                {/* <CardActions>
                    <Button size="small">Learn More</Button>
                </CardActions> */}
                </Card>
                
                <Card className='Homepage1-card' style={{marginTop: '30px',marginLeft: '-10px',backgroundColor: "#d6fbfd" ,}}>
                <Grid className='Homepage-lovedocImage'>
                    <img alt=''/>
                </Grid>
                <CardContent style={{marginTop: '-30px'}}>
                    <Typography variant='h5' className='Homepage-card-title' gutterBottom>
                    <BeenhereIcon className='Homepage-icon' /><strong>Get Result Online</strong>
                    </Typography>
                    <Typography className='Homepage1-card-pos' color="textSecondary">
                    Lorem ipsum dolor sit amet,<br/>
                     consectetur adipiscing elit, sed do
                    </Typography>
                </CardContent>
                
                {/* <CardActions>
                    <Button size="small">Learn More</Button>
                </CardActions> */}
                </Card>
            </Grid>
        </Grid>
    );
}