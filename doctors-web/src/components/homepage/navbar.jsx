import React from "react";
import {
     Container,
     Grid,
    Typography,
    Button,
    Card,
    CardActions,
    CardContent,

 } from '@material-ui/core';
 import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';
 import CallIcon from '@material-ui/icons/Call';
 import EmailIcon from '@material-ui/icons/Email';
 import { Navbar } from 'react-bootstrap';
  import FacebookIcon from '@material-ui/icons/Facebook';
 import TwitterIcon from '@material-ui/icons/Twitter';
 import LinkedInIcon from '@material-ui/icons/LinkedIn';
 import YouTubeIcon from '@material-ui/icons/YouTube';
 import InstagramIcon from '@material-ui/icons/Instagram';
 import './styles.scss';
import logo from '../images/logo.png';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
  }));

export default function NavBar() {
   const classes = useStyles();

     return(
            <>
          <Grid container direction="row" lg={12} md={12} xs={12} spacing={2} className="nav">
          <Grid item lg={8} md={9} xs={6} spacing={2} className='navbar1' style={{zIndex:"999", position:"relative"}}>
            
            <Grid item lg={2}>
                <AccessAlarmIcon className="icon"/>Mon-Fri 9am-5pm
             </Grid>
             <Grid item lg={2}>
                <CallIcon className="icon"/>Mon-Fri 9am-5pm
             </Grid>
              <Grid item lg={2}>
                <EmailIcon className="icon"/>Mon-Fri 9am-5pm
             </Grid>
             
             
            </Grid>
             <Grid item lg={4} md={3} xs={6} spacing={2} className='navbar2'>
                   <FacebookIcon className="icon"/><TwitterIcon className="icon"/><LinkedInIcon className="icon"/><YouTubeIcon className="icon" /><InstagramIcon className="icon" />

             </Grid>
             <AppBar className='NavBar-toolBar' style={{backgroundColor: "black",zIndex:"1030",  position: "fixed",padding:" 12px 15px",}}>
            <Toolbar>
            <IconButton edge="start" className={classes.menuButton} color="black" aria-label="menu">
                <img src={logo} alt="" className='Homepage1-image'/>
            </IconButton>
            <Typography variant="h6" color="inherit">
                Home
            </Typography>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Typography variant="h6" color="inherit">
                Services
            </Typography>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Typography variant="h6" color="inherit">
                Doctors
            </Typography>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Typography variant="h6" color="inherit">
                About
            </Typography>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <Typography variant="h6" color="inherit">
                Contact
            </Typography>
            </Toolbar>
        </AppBar>
            
          </Grid>
         
         
       </>

     );
}