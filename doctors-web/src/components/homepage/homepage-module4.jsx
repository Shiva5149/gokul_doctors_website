import React, { useState } from 'react';
import { createMuiTheme } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Gokul1 from '../images/Gokul1.png';
import Gokul2 from '../images/Gokul-srikanth.png';
import Gokul3 from '../images/Gokul-srikanthbdwj.png';
import Gokul4 from '../images/Gokul-MD.png';
import Gokul5 from '../images/Gokul-divya.png';
import Gokul6 from '../images/Gokul-john.png';
import Gokul7 from '../images/Goukul-krishna.png';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import { red } from '@material-ui/core/colors';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import InstagramIcon from '@material-ui/icons/Instagram';
import PinterestIcon from '@material-ui/icons/Pinterest';



const theme = createMuiTheme ({
    palette: {
        primary:{
            main:'#3c52b2'
        }
    }
})

const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));

export default function HomePage4(props) {
    const classes = useStyles();
    const [autoPlay, setAutoPlay] = useState(true);
    const responsive = {
        superLargeDesktop: {
          // the naming can be any, depends on you.
          breakpoint: { max: 4000, min: 3000 },
          items: 5
        },
        desktop: {
          breakpoint: { max: 3000, min: 1024 },
          items: 3
        },
        tablet: {
          breakpoint: { max: 1024, min: 464 },
          items: 2
        },
        mobile: {
          breakpoint: { max: 464, min: 0 },
          items: 1
        }
      };

      


    return(
        <Grid container item lg={12} md={12} xs={12} className='Homepage4-main'>
            <Typography>Hi HomePage4</Typography>
            
            <Grid item lg={6} md={12} xs={12}>
            <Typography variant='h6' style={{color:'blue'}}><strong>Our Doctors</strong></Typography><br/>
                <Typography variant='h3' style={{}}><strong>Our Specialist Doctors </strong> </Typography><br/>
                <Typography>Lorem ipsum dolor sit amet consectetur adipisicing elit.
                     A facilis vel consequatur tempora atque blanditiis exercitationem incidunt,
                      alias corporis quam assumenda dicta, temporibus.</Typography>
            </Grid>
            <Grid item lg={6} md={12} xs={12} className='Homepage-carousel'>
            <Carousel
            swipeable={false}
            draggable={false}
            showDots={true}
            responsive={responsive}
            ssr={true} // means to render carousel on server-side.
            infinite={true}
            autoPlay={props.deviceType !== "mobile" ? true : false}
            autoPlaySpeed={5000}
            keyBoardControl={true}
            customTransition="all .5"
            transitionDuration={500}
            containerClass="carousel-container"
            removeArrowOnDeviceType={["tablet", "mobile"]}
            deviceType={props.deviceType}
            dotListClass="custom-dot-list-style"
            itemClass="carousel-item-padding-40-px"
            style={{spacing: "50px"}}
            >
                <Card>
                <CardMedia
                        className={classes.media}
                        image={Gokul1}
                        title="Paella dish"
                    />
                    <CardContent>
                        <Typography variant="h4"><strong>Name</strong></Typography>
                        <Typography variant="h6">Specialization</Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                        This impressive paella is a perfect party dish and a fun meal to cook together with your
                        guests. Add 1 cup of frozen peas along with the mussels, if you like.
                        </Typography>
                    </CardContent>
                    <CardActions disableSpacing>
                        <IconButton aria-label="facebook">
                        <FacebookIcon />
                        </IconButton>
                        <IconButton aria-label="twitter">
                        <TwitterIcon />
                        </IconButton>
                        <IconButton
                        aria-label="instagram"
                        >
                        <InstagramIcon />
                        </IconButton>
                        <IconButton
                        aria-label="pinterest"
                        >
                        <PinterestIcon />
                        </IconButton>
                    </CardActions>
                </Card>
                <Card>
                <CardMedia
                        className={classes.media}
                        image={Gokul2}
                        title="Paella dish"
                    />
                    <CardContent>
                        <Typography variant="h4"><strong>Name</strong></Typography>
                        <Typography variant="h6">Specialization</Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                        This impressive paella is a perfect party dish and a fun meal to cook together with your
                        guests. Add 1 cup of frozen peas along with the mussels, if you like.
                        </Typography>
                    </CardContent>
                    <CardActions disableSpacing>
                        <IconButton aria-label="facebook">
                        <FacebookIcon />
                        </IconButton>
                        <IconButton aria-label="twitter">
                        <TwitterIcon />
                        </IconButton>
                        <IconButton
                        aria-label="instagram"
                        >
                        <InstagramIcon />
                        </IconButton>
                        <IconButton
                        aria-label="pinterest"
                        >
                        <PinterestIcon />
                        </IconButton>
                    </CardActions>
                </Card>
                <Card>
                <CardMedia
                        className={classes.media}
                        image={Gokul3}
                        title="Paella dish"
                    />
                    <CardContent>
                        <Typography variant="h4"><strong>Name</strong></Typography>
                        <Typography variant="h6">Specialization</Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                        This impressive paella is a perfect party dish and a fun meal to cook together with your
                        guests. Add 1 cup of frozen peas along with the mussels, if you like.
                        </Typography>
                    </CardContent>
                    <CardActions disableSpacing>
                        <IconButton aria-label="facebook">
                        <FacebookIcon />
                        </IconButton>
                        <IconButton aria-label="twitter">
                        <TwitterIcon />
                        </IconButton>
                        <IconButton
                        aria-label="instagram"
                        >
                        <InstagramIcon />
                        </IconButton>
                        <IconButton
                        aria-label="pinterest"
                        >
                        <PinterestIcon />
                        </IconButton>
                    </CardActions>
                </Card>
                <Card>
                <CardMedia
                        className={classes.media}
                        image={Gokul4}
                        title="Paella dish"
                    />
                    <CardContent>
                        <Typography variant="h4"><strong>Name</strong></Typography>
                        <Typography variant="h6">Specialization</Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                        This impressive paella is a perfect party dish and a fun meal to cook together with your
                        guests. Add 1 cup of frozen peas along with the mussels, if you like.
                        </Typography>
                    </CardContent>
                    <CardActions disableSpacing>
                        <IconButton aria-label="facebook">
                        <FacebookIcon />
                        </IconButton>
                        <IconButton aria-label="twitter">
                        <TwitterIcon />
                        </IconButton>
                        <IconButton
                        aria-label="instagram"
                        >
                        <InstagramIcon />
                        </IconButton>
                        <IconButton
                        aria-label="pinterest"
                        >
                        <PinterestIcon />
                        </IconButton>
                    </CardActions>
                </Card>
                <Card>
                <CardMedia
                        className={classes.media}
                        image={Gokul5}
                        title="Paella dish"
                    />
                
                <CardContent>
                        <Typography variant="h4"><strong>Name</strong></Typography>
                        <Typography variant="h6">Specialization</Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                        This impressive paella is a perfect party dish and a fun meal to cook together with your
                        guests. Add 1 cup of frozen peas along with the mussels, if you like.
                        </Typography>
                    </CardContent>
                    <CardActions disableSpacing>
                        <IconButton aria-label="facebook">
                        <FacebookIcon />
                        </IconButton>
                        <IconButton aria-label="twitter">
                        <TwitterIcon />
                        </IconButton>
                        <IconButton
                        aria-label="instagram"
                        >
                        <InstagramIcon />
                        </IconButton>
                        <IconButton
                        aria-label="pinterest"
                        >
                        <PinterestIcon />
                        </IconButton>
                    </CardActions>
                    </Card>
                <Card>
                <CardMedia
                        className={classes.media}
                        image={Gokul6}
                        title="Paella dish"
                    />
                    <CardContent>
                        <Typography variant="h4"><strong>Name</strong></Typography>
                        <Typography variant="h6">Specialization</Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                        This impressive paella is a perfect party dish and a fun meal to cook together with your
                        guests. Add 1 cup of frozen peas along with the mussels, if you like.
                        </Typography>
                    </CardContent>
                    <CardActions disableSpacing>
                        <IconButton aria-label="facebook">
                        <FacebookIcon />
                        </IconButton>
                        <IconButton aria-label="twitter">
                        <TwitterIcon />
                        </IconButton>
                        <IconButton
                        aria-label="instagram"
                        >
                        <InstagramIcon />
                        </IconButton>
                        <IconButton
                        aria-label="pinterest"
                        >
                        <PinterestIcon />
                        </IconButton>
                    </CardActions>
                </Card>
                <Card>
                <CardMedia
                        className={classes.media}
                        image={Gokul7}
                        title="Paella dish"
                    />
                    <CardContent>
                        <Typography variant="h4"><strong>Name</strong></Typography>
                        <Typography variant="h6">Specialization</Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                        This impressive paella is a perfect party dish and a fun meal to cook together with your
                        guests. Add 1 cup of frozen peas along with the mussels, if you like.
                        </Typography>
                    </CardContent>
                    <CardActions disableSpacing>
                        <IconButton aria-label="facebook">
                        <FacebookIcon />
                        </IconButton>
                        <IconButton aria-label="twitter">
                        <TwitterIcon />
                        </IconButton>
                        <IconButton
                        aria-label="instagram"
                        >
                        <InstagramIcon />
                        </IconButton>
                        <IconButton
                        aria-label="pinterest"
                        >
                        <PinterestIcon />
                        </IconButton>
                    </CardActions>
                </Card>
            </Carousel>;
            </Grid>

        </Grid>
    );
}