import React from 'react';
import {
    Grid,
   Typography,
   Button,
   Card,
   CardActions,
   CardContent,

} from '@material-ui/core';
import './styles.scss';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import PhoneInTalkIcon from '@material-ui/icons/PhoneInTalk';

export default function ContactUs(){
    return(
        <Grid container item lg={12} md={12} xs={12} className='Contact-home'>
            <Grid item lg={6} md={6} xs={6} className="Contactus-left">
                <Typography variant='h4'>Emergency? For any Help Contact Us Now</Typography>
                <Typography color='textSecondary'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
                sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                 Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas
                  accumsan lacus vel facilisis. Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                   Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                     sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                      Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas 
                      accumsan lacus vel facilisis.
                      </Typography>
                      <PhoneInTalkIcon/><Typography variant="h6">Call Now:+821-456-789</Typography>
                      <MailOutlineIcon/><Typography variant="h6">Mail us:hello@info.com</Typography>

            </Grid>
            <Grid item lg={6} md={6} xs={6} className="Contactus-right">
                <p>Contact</p>
            </Grid>
        </Grid>
    )
}